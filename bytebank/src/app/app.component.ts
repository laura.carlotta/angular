import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
}) /* decorator */

export class AppComponent {
	title = 'bytebank';
	/* transferency: any = {}; */
	transfers: any[] = [];

	/* REFATORAMOS
	destiny!: number;
	valueTransf!: number; */

	transfer($event: any) {
		console.log($event);
		/* REFATORAMOS
		this.destiny = $event.destiny;
		this.valueTransf = $event.valueTransf; */
		const transferency = {...$event, date: new Date()};
		this.transfers.push(transferency);
	}
} /* classe */
