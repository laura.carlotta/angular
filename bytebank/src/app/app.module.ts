import { NewTranferenceComponent } from './new-transference/new-transference-component';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core'; /* para formatação de data */
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ExtractComponent } from './extract/extract.component'; /* Api que importamos (biblioteca para formulários) */
import { registerLocaleData } from '@angular/common'; /* para formatação de data */
import localePt from '@angular/common/locales/pt'; /* para formatação de data */

registerLocaleData(localePt, 'pt');

@NgModule({
	declarations: [
		AppComponent,
		NewTranferenceComponent,
  		ExtractComponent
	],
	imports: [
		BrowserModule,
		FormsModule
	],
	providers: [
		{ provide: LOCALE_ID, useValue: 'pt' },
		{
			provide: DEFAULT_CURRENCY_CODE,
			useValue: 'BRL',
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
