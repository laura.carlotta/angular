/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TransferencyService } from './transferency.service';

describe('Service: Transferency', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransferencyService]
    });
  });

  it('should ...', inject([TransferencyService], (service: TransferencyService) => {
    expect(service).toBeTruthy();
  }));
});
