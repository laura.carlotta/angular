
import { Component, Output, EventEmitter } from "@angular/core";


@Component({
	selector: 'app-new-tranference',
	templateUrl: './new-transference-component.html',
	styleUrls: ['./new-transference-component.scss']
})
export class NewTranferenceComponent {

	@Output() totransfer = new EventEmitter<any>();
	/* @Output() totransfer: EventEmitter<any> = new EventEmitter(); */

	valueTransf!: number;
	destiny!: number; /* Já vem com o valor mostrado no campo */

	transfer() {
		/* console.log('cliquei');
		console.log('Valor: ', this.valueTransf);
		console.log(typeof this.valueTransf);
		console.log('Destino:', this.destiny);
		console.log(typeof this.destiny); */

		console.log('solicitada nova tranferência');

		const emitValue = { valueTransf: this.valueTransf, destiny: this.destiny }; /* => o que está dentro do emit poderia ser representado assim, como uma variável */
		this.totransfer.emit(emitValue); /* => ou poderia passar o valor direto dentro das chaves */

		this.cleanInputs();
	}


	cleanInputs () {
		this.valueTransf = 0;
		this.destiny = 0;
	}
}
